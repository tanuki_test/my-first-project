Option Explicit

Sub import_macro()

Const impFile As String = "basファイルの絶対パス"

Dim importer As New ModuleImporter

With importer
  If .existsModule(impFile) Then
   .removeModule (.baseName(impFile))
   .importBas (impFile)
  Else
   .importBas (impFile)
  End If
End With

Set importer = Nothing

'読み込んだモジュール下記のように指定すると更新処理と同時に実行できる
'Application.Run "PERSONAL.XLSB!" & "モジュール名"

End Sub