'ModuleImpoter
'このクラスは、.basファイルをブックへインポートするためのメソッドを提供します。
Option Explicit


'絶対パスを受け取り拡張子を除いたファイル名を返します。
'
Public Function baseName(path As String) As String

  Dim f As Object
  Set f = CreateObject("Scripting.FileSystemObject")
  baseName = f.getBaseName(path)

End Function


'指定したモジュールを開放します。
'
Public Function removeModule(moduleName As String)
 
  Dim Obj As VBIDE.VBProject
  Set Obj = ThisWorkbook.VBProject

  Call Obj.VBComponents.Remove(Obj.VBComponents(moduleName))

  Set Obj = Nothing

End Function


'指定したbasファイルを現在のブックへインポートします。
'
Public Function importBas(filePath As String)

 Dim Obj As VBIDE.VBProject
 Set Obj = ThisWorkbook.VBProject

 Obj.VBComponents.Import filePath
  
 Set Obj = Nothing
 
End Function


'指定したファイルのモジュールが存在するか判定します。
'
Public Function existsModule(impFilePath As String) As Boolean

  existsModule = False
  
  Dim fileBaseName As String
  fileBaseName = baseName(impFilePath)

  Dim Obj As VBIDE.VBProject
  Set Obj = ThisWorkbook.VBProject

  Dim CompCnt As Long
  CompCnt = Obj.VBComponents.Count

  Dim lp As Long
  For lp = 1 To CompCnt
    If Obj.VBComponents(lp).Name = fileBaseName Then
      existsModule = True
     GoTo END_JUDGE
    End If
  Next
 
END_JUDGE:

  Set Obj = Nothing

End Function
